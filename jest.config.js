module.exports = {
  clearMocks: true,
  setupFilesAfterEnv: [`<rootDir>/tests/test_setup.js`],
  testMatch: ["**/*.test.js"],
  transform: {
    "\\.html$": "jest-raw-loader",
    "\\.js$": "babel-jest",
  },
};
