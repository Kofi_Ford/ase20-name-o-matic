const plugins = [];

let presets = [
  [
    "@babel/preset-env",
    {
      useBuiltIns: "usage",
      corejs: { version: 3, proposals: true },
      modules: false,
    },
  ],
];

const isJest = Boolean(process.env.JEST_WORKER_ID);
if (isJest) {
  plugins.push("@babel/plugin-transform-modules-commonjs");

  presets = [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current",
        },
      },
    ],
  ];
}

module.exports = { presets, plugins, sourceType: "unambiguous" };
