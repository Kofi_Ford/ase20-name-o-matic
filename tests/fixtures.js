import { memoize } from "lodash";
import indexHtml from "../public/index.html";

export const getIndexHtmlBody = memoize(() => {
  const parser = new DOMParser();
  const doc = parser.parseFromString(indexHtml, "text/html");

  // Clean
  doc.body.querySelectorAll("script").forEach((x) => x.remove());

  return doc.body.innerHTML;
});
