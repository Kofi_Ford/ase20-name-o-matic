# (ASE 2020) Name o' Matic

This is a homework project for Advanced Software Engineering 2020.

![screenshot](./screenshot.png)

## How to run locally?

Simply open up the `public/index.html` file in your browser.
As you make changes locally, be sure to refresh the page in your browser.

## Homework

- [Homework 1 - Intro](./docs/HW_1_INTRO.md)
- [Homework 2 - Testing](./docs/HW_2_TESTING.md)
